/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Image;

/**
 *
 * @author Malikha Duarte
 */
public class Inicio {
    
    public class Pecado{
       public static final  String Titulo = "El Hijo de Dios nos ha amado"+
                      " primero, “siendo nosotros "+
                      "  todavía pecadores. Rom 5,8";
    }
    
    public class Permanecer{
       public static final  String Titulo1 = "Permanecer en Cristo no es algo pasivo, requiere acción de nuestra parte.";
    }
    public class Regeneracion{
       public static final  String Titulo2 = "La regeneración tiene como su idea básica nacer de nuevo o ser restaurado.";
    }
    public class Rompiendo{
       public static final  String Titulo3 = "Dios es mi Camino, mi Verdad y mi Vida.";
    }
    public class Servicio{
       public static final  String Titulo4 = "Dios es mi Abogado, mi Mejor Amigo, mi Salvador. ";
    }
    public class Ultimo{
       public static final  String Titulo5 = "Dios es mi salvación y mi gloria, es la roca que me protege y fortalece.";
    }
    
    
   
}
